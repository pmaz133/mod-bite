//
//  Game.swift
//  Mod Bite
//
//  Created by Patryk Mazurkiewcz on 01/02/2020.
//  Copyright © 2020 Patryk Mazurkiewcz. All rights reserved.
//

import Foundation

struct Game {
    let isStarted: Bool
    let time: Int
    let score: Int
    let speed: Int
    let bites: Int
    let position: Int
}
